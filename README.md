# Simple GoCD server

Sample GoCD server with reverse proxy for the Docker environment.

## Prerequisites

Make sure you have already installed Docker Engine and Compose tool.

```
$ docker --version
Docker version 20.10.7, build f0df350

$ docker-compose --version
docker-compose version 1.29.2, build 5becea4c
```

## Installing

```
git clone https://bitbucket.org/fayala/cicd_server_infra.git

cd cicd_server_infra/
$ docker-compose up -d

```
Go to [http://localhost](http://localhost)
